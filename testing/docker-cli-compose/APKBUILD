# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.0.0_beta6
_ver=2.0.0-beta.6
pkgrel=0
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="compose-cli-$_ver.tar.gz::https://github.com/docker/compose-cli/archive/v$_ver.tar.gz"

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-cli-"$_ver"

export GOPATH=$srcdir/go
export GOCACHE=$srcdir/go-build
export GOTMPDIR=$srcdir

build() {
	PKG=github.com/docker/compose-cli
	local ldflags="-s -w -X $PKG/internal.Version=v$_ver"
	go build -modcacherw -ldflags "$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list -modcacherw ./... | grep -Ev '/e2e(/|$)')"
	go test -modcacherw -short $pkgs
	./docker-compose compose version
}

package() {
	install -Dm755 docker-compose "$pkgdir$_plugin_installdir"/docker-compose
}

sha512sums="
2bf39813797ac58f1b29b329ee396ca6c52fb6cafa2a53b60ca6a1275947c3c663903af4a1a7e20339bab2ae76e847f63071d879e8549a51ea68c50ff2dbb0eb  compose-cli-2.0.0-beta.6.tar.gz
"
