# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-faker
_pyname=Faker
pkgver=8.10.2
pkgrel=0
pkgdesc="Python package that generates fake data for you"
url="https://faker.readthedocs.io/en/master"
arch="noarch"
license="MIT"
depends="py3-dateutil py3-six py3-text-unidecode"
makedepends="py3-setuptools"
checkdepends="py3-email-validator py3-ipaddress py3-mock py3-freezegun
	py3-more-itertools py3-pytest py3-ukpostcodeparser py3-validators
	py3-pytest-runner py3-random2 py3-dateutil py3-text-unidecode
	py3-freezegun py3-validators py3-pillow"
_pypiprefix="${_pyname%${_pyname#?}}"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir=$srcdir/$_pyname-$pkgver

replaces="py-faker" # Backwards compatibility
provides="py-faker=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# tests erroneously require a specific version of pytest
	sed -i setup.py -e 's/ *"pytest>=.*//g'
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
5a148327df7c34623c0d8c154e6181a183a0aa484659e5872013f0b9c510418690278550ebbed6eb984c87cef588fd862b213295a379b5be3433afe312181103  Faker-8.10.2.tar.gz
"
